+----------------------------------------+
| Author:       Sander van Rijn          |
| Email:        svr003@gmail.com         |
|                                        |
| Institute:    LIACS, Leiden University |
| Project:      Modular CMA-ES Framework |
+----------------------------------------+

# Summary #
This repository contains the code for the modular CMA-ES framework by Sander van Rijn.

# Documentation #
Documentation is available at http://www.svrijn.nl/modular-cma-es-framework/docs

# References #
Evolving the Structure of Evolution Strategies. Sander van Rijn, Hao Wang, Matthijs van Leeuwen,
Thomas Bäck. IEEE SSCI December 6-9 2016, Athens.